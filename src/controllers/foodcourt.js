const FoodCourt = require('../models/FoodCourt');
const bcrypt = require('bcryptjs');
const passport = require('passport');

module.exports = {
    create: (req, res) => {

        const {
            fc_name,
            fc_email,
            password,
            password2
        } = req.body;

        // const fc_name = req.body["fc_name"];
        // const fc_email = req.body["fc_email"];
        // const password = req.body["password"];
        // const password2 = req.body["password2"];

        let errors = [];

        if (!fc_name || !fc_email || !password) {
            errors.push({
                msg: 'Enter all fields'
            });
        }

        if (password != password2) {
            errors.push({
                msg: "Passwords dont match"
            });
        }

        if (password.length < 6) {
            errors.push({
                msg: "Password must be atleast 6 characters"
            });
        }

        if (errors.length > 0) {
            return res.render('register', {
                errors,
                fc_name,
                fc_email,
                password,
                password2
            });
        } else {
            FoodCourt.findOne({
                    fc_email: fc_email
                })
                .then(fc => {
                    if (fc) {
                        errors.push({
                            msg: 'Email already exists'
                        });
                        res.render('/register', {
                            errors,
                            fc_name,
                            fc_email,
                            password,
                            password2
                        })
                    } else {
                        const newFc = new FoodCourt({
                            fc_name,
                            fc_email,
                            password
                        });

                        bcrypt.genSalt(10, (err, salt) => {
                            bcrypt.hash(newFc.password, salt, (err, hash) => {
                                if (err) throw err;

                                newFc.password = hash;
                                newFc.save()
                                    .then(fc => {
                                        req.flash('success_msg', 'Registered');
                                        res.redirect('login');
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    })
                            });
                        });
                    }
                });
        }
    },

    all: (req, res) => {
        FoodCourt.find(null, 'id fc_name', (err, docs) => {
            if (err) throw err;
            return res.send(docs);
        })
        //return res.send(fcs[0]);
    },

    login: (req, res, next) => {
        passport.authenticate('local', {
            successRedirect: '/dashboard',
            failureRedirect: '/login',
            failureFlash: true
        })(req, res, next);
    },

    logout: (req, res) => {
        req.logout();
        req.flash('success_msg', 'You are logged out');
        res.redirect('/login');
    }
}