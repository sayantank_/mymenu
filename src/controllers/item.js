const Item = require('../models/Items');

function formatDate(date) {
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
  
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
  
    return year + '-' + monthNames[monthIndex] + '-' + day;
}

module.exports = {
    
    create: (req, res) => {

        let errors = [];
        let items = [];

        let date = req.body.date;
        if(date === ''){
            errors.push("Please enter a date");
        }

        let item_meal = req.body.item_meal;

        let item_fc = req.user._id;

        if(errors.length > 0){
            res.render('dashboard', {
                fc: req.user,
                errors,
                date: formatDate(new Date())
            })
        } else {
            for(var i = 1; i < 10; i++){
                let item_name = req.body[`item${i}`];
                if(item_name === ''){
                    continue;
                } else {
                    let newItem = new Item({
                        item_name,
                        item_fc,
                        item_meal,
                        date
                    });
                    newItem.save()
                        .then(item => {
                            console.log(`Saved ${item.item_name}`)
                        })
                        .catch(err => {
                            console.log(err);
                        })
                }
            }
            req.flash('success_msg', 'Items saved');
            res.redirect('dashboard');
        }
    }

}