function showModal() {
  console.log("hello");
  let date = document.getElementById("menu_date").value;
  if (date === undefined || date === null || date === "") {
    alert("Select date");
  } else {
    let modal = document.getElementById("modal");
    let header = document.getElementById("modal-header");
    header.innerHTML = date;

    let breakfast_items = document
      .getElementById("breakfast")
      .getElementsByTagName("input");
    let lunch_items = document
      .getElementById("lunch")
      .getElementsByTagName("input");
    let snacks_items = document
      .getElementById("snacks")
      .getElementsByTagName("input");
    let dinner_items = document
      .getElementById("dinner")
      .getElementsByTagName("input");

    let breakfast = [];
    let lunch = [];
    let snacks = [];
    let dinner = [];

    for (var i = 0; i < breakfast_items.length; i++) {
      if (
        breakfast_items[i].value === undefined ||
        breakfast_items[i].value === null ||
        breakfast_items[i].value === ""
      ) {
        continue;
      } else {
        breakfast.push(breakfast_items[i].value);
      }
    }
    for (var i = 0; i < lunch_items.length; i++) {
      if (
        lunch_items[i].value === undefined ||
        lunch_items[i].value === null ||
        lunch_items[i].value === ""
      ) {
        continue;
      } else {
        lunch.push(lunch_items[i].value);
      }
    }
    for (var i = 0; i < snacks_items.length; i++) {
      if (
        snacks_items[i].value === undefined ||
        snacks_items[i].value === null ||
        snacks_items[i].value === ""
      ) {
        continue;
      } else {
        snacks.push(snacks_items[i].value);
      }
    }
    for (var i = 0; i < dinner_items.length; i++) {
      if (
        dinner_items[i].value === undefined ||
        dinner_items[i].value === null ||
        dinner_items[i].value === ""
      ) {
        continue;
      } else {
        dinner.push(dinner_items[i].value);
      }
    }

    for (var i = 0; i < breakfast.length; i++) {
      let list = document.getElementById("modal-breakfast");
      let item_div = document.createElement("div");
      item_div.className = "modal-item";
      item_div.id = `breakfast${i}`;
      item_div.textContent = breakfast[i];
      list.append(item_div);
    }
    for (var i = 0; i < lunch.length; i++) {
      let list = document.getElementById("modal-lunch");
      let item_div = document.createElement("div");
      item_div.className = "modal-item";
      item_div.id = `lunch${i}`;
      item_div.textContent = lunch[i];
      list.append(item_div);
    }
    for (var i = 0; i < snacks.length; i++) {
      let list = document.getElementById("modal-snacks");
      let item_div = document.createElement("div");
      item_div.className = "modal-item";
      item_div.id = `snacks${i}`;
      item_div.textContent = snacks[i];
      list.append(item_div);
    }
    for (var i = 0; i < dinner.length; i++) {
      let list = document.getElementById("modal-dinner");
      let item_div = document.createElement("div");
      item_div.className = "modal-item";
      item_div.id = `dinner${i}`;
      item_div.textContent = dinner[i];
      list.append(item_div);
    }

    modal.style.display = "block";
  }
}
