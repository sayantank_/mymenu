const express = require('express');
const router = express.Router();
const FoodCourt = require('../controllers/foodcourt');
const Item = require('../controllers/item');

function formatDate(date) {
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return year + '-' + monthNames[monthIndex] + '-' + day;
}

const {
    forwardAuthenticated,
    ensureAuthenticated
} = require('../config/auth');

router.post('/items', Item.create);

router.get('/register', forwardAuthenticated, (req, res) => res.render('register'));
router.post('/register', FoodCourt.create);

router.get('/login', forwardAuthenticated, (req, res) => res.render('login'));
router.post('/login', FoodCourt.login);

router.get('/', forwardAuthenticated, (req, res) => res.render('login'));
router.get('/dashboard', ensureAuthenticated, (req, res) => res.render('dashboard', {
    fc: req.user,
    date: formatDate(new Date())
}));

router.get('/logout', FoodCourt.logout);



module.exports = router;