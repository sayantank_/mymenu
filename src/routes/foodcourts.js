const express = require('express');
const router = express.Router();
const FoodCourt = require('../controllers/foodcourt');

router.get('/', FoodCourt.all);

module.exports = router;