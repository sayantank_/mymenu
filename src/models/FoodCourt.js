const mongoose = require('mongoose');

const FoodCourtSchema = new mongoose.Schema({
    fc_name: {
        type: String,
        required: true
    },
    fc_email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

const FoodCourt = mongoose.model('FoodCourt', FoodCourtSchema);

module.exports = FoodCourt;