const mongoose = require('mongoose');


const ItemSchema = mongoose.Schema({
    item_name: {
        type: String,
        required: true
    },
    item_fc: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FoodCourt',
        required: true
    },
    item_meal: {
        type: String,
        enum: ['Breakfast', 'Lunch', 'Snacks', 'Dinner'],
        required: true
    },
    date: {
        type: String,
        required: true
    }
});

const Item = mongoose.model('Item', ItemSchema);

module.exports = Item;