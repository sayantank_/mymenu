const mongoose = require('mongoose');

const MenuSchema = mongoose.Schema({
    menu_fc: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'FoodCourt',
        required: true
    },
    menu_date: {
        type: String,
        required: true
    },
    breakfast: {
        type: [String],
        default: undefined
    },
    lunch: {
        type: [String],
        default: undefined
    },
    snacks: {
        type: [String],
        default: undefined
    },
    dinner: {
        type: [String],
        default: undefined
    }
});

const Menu = mongoose.model('Menu', MenuSchema);

module.exports = Menu;