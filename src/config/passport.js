const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

const FoodCourt = require('../models/FoodCourt');

module.exports = function(passport) {

    // passport.use(
    //     new LocalStrategy({ usernameField: 'fc_email' }), (fc_email, password, done) => {
    //         FoodCourt.findOne({ fc_email: fc_email }, (err, fc) => {
    //             if(err) { return done(err); }
    //             if(!fc) {
    //                 return done(null, false, { message: 'Incorrect email' });
    //             }

    //             if(!fc.validPassword(password)) {
    //                 return done(null, false, { message: 'Incorrect password' });
    //             }

    //             return done(null, fc);
    //         });
    //     }
    // );
    
    passport.use(
        new LocalStrategy({ usernameField: 'fc_email' }, (fc_email, password, done) => {
            FoodCourt.findOne({
                fc_email: fc_email
            }).then(fc => {
                //console.log(fc.fc_email);
                if(!fc) {
                    return done(null, false, { message: 'That email is not registered' });
                }

                bcrypt.compare(password, fc.password, (err, isMatch) => {
                    if(err) throw err;
                    if(isMatch){
                        return done(null, fc);
                    } else {
                        return done(null, false, { message: 'Password Incorrect' });
                    }
                });
            });
        })
    );

    passport.serializeUser(function(fc, done) {
        done(null, fc.id);
    });

    passport.deserializeUser(function(id, done) {
        FoodCourt.findById(id, function(err, fc) {
            done(err, fc);
        });
    });
};